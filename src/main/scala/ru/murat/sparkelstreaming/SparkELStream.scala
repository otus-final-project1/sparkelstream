package ru.murat.sparkelstreaming

import org.apache.commons.math3.util.FastMath

import scala.math.random
import org.apache.spark.sql.{DataFrame, Dataset, Encoders, Row, SparkSession}
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions.{col, from_json, lit, udf}
import org.apache.spark.sql.streaming.Trigger
import org.orekit.bodies.{GeodeticPoint, OneAxisEllipsoid}
import org.orekit.data.{DataContext, DataProvidersManager, DirectoryCrawler}
import org.orekit.frames.FramesFactory
import org.orekit.propagation.analytical.tle.{TLE, TLEPropagator}
import org.orekit.time.{AbsoluteDate, TimeScalesFactory}
import org.orekit.utils.{Constants, IERSConventions, PVCoordinates}
import ru.murat.sparkelstreaming.model.{ISS, SpaceShip}

import java.io.File
import java.time.LocalDateTime


object SparkELStream {

  val tleToLLAOperationUDF = udf( (tle:String) => {
    val tles =  tle.split("\\r\\n")
    val tleaActual = new TLE(tles(0),tles(1))
    val now = LocalDateTime.now
    // Создание пропагатора из TLE// Создание пропагатора из TLE
    val propagator: TLEPropagator = TLEPropagator.selectExtrapolator(tleaActual)
    // Выбор даты для прогнозирования положения// Выбор даты для прогнозирования положения
    val targetDate = new AbsoluteDate(now.getYear,
      now.getMonthValue,
      now.getDayOfMonth,
      now.getHour,
      now.getMinute,
      now.getSecond,
      TimeScalesFactory.getUTC)
    // Получение положения и скорости в заданное время// Получение положения и скорости в заданное время
    val pvCoordinates: PVCoordinates = propagator.getPVCoordinates(targetDate, FramesFactory.getTEME)
    // Земная модель WGS84
    val earth = new OneAxisEllipsoid(Constants.WGS84_EARTH_EQUATORIAL_RADIUS, Constants.WGS84_EARTH_FLATTENING, FramesFactory.getITRF(IERSConventions.IERS_2010, true))
    // Преобразование из в геодезические координаты// Преобразование из в геодезические координаты
    val lla: GeodeticPoint = earth.transform(pvCoordinates.getPosition, FramesFactory.getTEME, targetDate)
    Array(FastMath.toDegrees(lla.getLongitude),FastMath.toDegrees(lla.getLatitude),lla.getAltitude/1000)
  })


  def saveISSToDB = (df: Dataset[SpaceShip], batchId: Long) => {
    val url = "jdbc:postgresql://localhost:5432/spaceships"
      df.write.format("jdbc")
      .option("url", url)
      .option("dbtable", "spaceships")
      .option("user", "admin")
      .option("password", "admin")
        .mode("append")
        .save()
        }

  def saveCSSToDB = (df: Dataset[SpaceShip], batchId: Long) => {
    val url = "jdbc:postgresql://localhost:5432/spaceships"
      df.write.format("jdbc")
      .option("url", url)
      .option("dbtable", "spaceships")
      .option("user", "admin")
      .option("password", "admin")
      .mode("append")
      .save()
  }


  val ISS = "iss"
  val CSS = "css"

  def main(args: Array[String]): Unit = {

    val orekitData = new File("src/main/resources/orekit-data")
    val manager: DataProvidersManager = DataContext.getDefault.getDataProvidersManager
    manager.addProvider(new DirectoryCrawler(orekitData))

    val spark = SparkSession
      .builder
      .appName("SpaceshipsStreaming")
      .master("local[*]")
      .getOrCreate()

    spark.sparkContext.setLogLevel("OFF")
    import spark.implicits._

    val issJsonSchema = Encoders.product[ISS].schema

    val cssMessageSchema = new StructType()
      .add("info",new StructType()
                  .add("satid",LongType)
                  .add("satname",StringType)
                  .add("transactionscount",StringType))
      .add("tle",StringType)


    val kafkaReadStreamDF = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "localhost:29092")
      .option("subscribe", "spaceships")
      .option("startingOffsets", "latest")
      .option("value.deserializer", "org.apache.kafka.common.serialization.StringSerializer")
      .option("key.deserializer", "org.apache.kafka.common.serialization.StringSerializer")
      .load()
      .selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
      .filter(!$"value".like("%error%"))



    val CSSwriteStream = kafkaReadStreamDF
      .filter($"key" === CSS)
      .withColumn("cssMessage", from_json($"value",cssMessageSchema))
      .withColumn("lla",tleToLLAOperationUDF($"cssMessage.tle"))
      .select($"cssMessage.info.satid".as("id"),
        $"cssMessage.info.satname".as("name"),
        $"lla"(0).as("longitude"),
        $"lla"(1).as("latitude"),
        $"lla"(2).as("altitude")
      )
      .as[SpaceShip]
      .writeStream
      .outputMode("append")
      .foreachBatch(saveCSSToDB).start()


    val ISSreadStream = kafkaReadStreamDF
      .filter($"key" === ISS)
      .withColumn("jsondata", from_json($"value", issJsonSchema))
      .drop($"value", $"key")
      .select($"jsondata.id".as("id"),
              $"jsondata.name".as("name"),
              $"jsondata.latitude".as("latitude"),
              $"jsondata.longitude".as("longitude"),
              $"jsondata.altitude".as("altitude"))
      .as[SpaceShip]
    .writeStream
    .outputMode("append")
    .foreachBatch(saveISSToDB)
    .start()


    spark.streams.awaitAnyTermination()
    spark.stop()
  }
}

