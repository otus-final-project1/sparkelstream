package ru.murat.sparkelstreaming.model

case class ISS(name: Option[String],
               id: Option[Long],
               latitude: Option[Double],
               longitude:Option[Double],
               altitude:Option[Double],
               velocity: Option[Double],
               visibility: Option[String],
               footprint: Option[String],
               timestamp: Option[Long],
               daynum: Option[Double],
               solar_lat: Option[Double],
               solar_lon: Option[Double], units: Option[String])





