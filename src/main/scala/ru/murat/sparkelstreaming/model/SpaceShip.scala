package ru.murat.sparkelstreaming.model

case class SpaceShip(name: Option[String],
                     id: Option[Long],
                     latitude: Option[Double],
                     longitude:Option[Double],
                     altitude:Option[Double])
